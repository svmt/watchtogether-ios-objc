//
//  AppDelegate.h
//  ExampleObjC
//
//  Created by Nikolas Omelianov on 20.08.2021.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end

