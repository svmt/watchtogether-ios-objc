//
//  SceneDelegate.h
//  ExampleObjC
//
//  Created by Nikolas Omelianov on 20.08.2021.
//

#import <UIKit/UIKit.h>

@interface SceneDelegate : UIResponder <UIWindowSceneDelegate>

@property (strong, nonatomic) UIWindow * window;

@end

