//
//  ObjcSessionViewController.m
//  Example
//
//  Created by George on 07.12.2020.
//

#import "ObjcSessionViewController.h"
#import "ParticipantCell.h"
#import <AVFoundation/AVFoundation.h>
#import <CallKit/CallKit.h>
#import "ObjcChatViewController.h"

@import WatchTogetherAdapter;

@interface ObjcSessionViewController () <SessionAdapterDelegate, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout>
@property (weak, nonatomic) IBOutlet UIButton * leaveButton;
//@property (weak, nonatomic) IBOutlet UILabel *receiveMessageLabel;

@property (nonatomic) LocalParticipantAdapter* localParticipant;
@property (nonatomic) SessionAdapter* session;
@property (nonatomic, strong) NSMutableArray* participants;
@property (nonatomic, strong) UIViewController *logsVC;
@property (nonatomic, strong) LocalParticipantSnapshotAdapter *localParticipantSnapshot;
@property (nonatomic) BOOL isRegularDisconnect;
@property (nonatomic) BOOL cameraStarted;
@property (nonatomic) CXCallObserver *callObserver;
@end

@implementation ObjcSessionViewController
 

- (void)viewDidLoad {
    [super viewDidLoad];
// MARK: Either use notification center for app state or CXCallDelegate specificaly for call events
// MARK: option with call observer
//    _callObserver = [CXCallObserver new];
//    [_callObserver setDelegate:self queue:nil];
    _participants = [[NSMutableArray alloc] init];
    _isRegularDisconnect = NO;
    _cameraStarted = NO;
    [self setupSession];
    
// MARK: option with notification center
    NSNotificationCenter* defaultCenter = [NSNotificationCenter defaultCenter];
    [defaultCenter addObserver:self selector:@selector(appMovedToBackground:) name: UIApplicationDidEnterBackgroundNotification object:nil];
    [defaultCenter addObserver:self selector:@selector(appDidBecomeActive:) name:UIApplicationWillEnterForegroundNotification object:nil];

    UIBarButtonItem *menuButton;
    if (@available(iOS 13.0, *)) {
         menuButton = [[UIBarButtonItem alloc] initWithImage:[UIImage systemImageNamed:@"ellipse"] style:UIBarButtonItemStylePlain target:self action:@selector(showMenu)];

    } else {
        menuButton = [[UIBarButtonItem alloc] initWithTitle:@"..." style:UIBarButtonItemStylePlain target:self action:@selector(showMenu)];
    };
    [self.navigationItem setRightBarButtonItem:menuButton];
}

- (void)showMenu {
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Additional functions" message:nil preferredStyle:UIAlertControllerStyleActionSheet];
    if (self.session.sessionState == SessionStateAdapterConnected) {
        UIAlertAction *actionChat = [UIAlertAction actionWithTitle:@"Open chat" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
            ObjcChatViewController *chatVC = [[ObjcChatViewController alloc] init];
            [self.navigationController pushViewController:chatVC animated:YES];
        }];
        [alert addAction:actionChat];
    }
    UIAlertAction *actionLog = [UIAlertAction actionWithTitle:@"Show log" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self presentViewController:self.logsVC animated:YES completion:nil];
    }];
    [alert addAction:actionLog];
    [self presentViewController:alert animated:YES completion:nil];

}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setHidden:NO];
}
- (IBAction)showLogsTapped:(UIButton *)sender {
    [self presentViewController:_logsVC animated:YES completion:nil];
}

- (IBAction)cameraPreviewAction:(UIButton*)sender {
//    [sender setHidden:YES];
    if (_cameraStarted) {
        [_localParticipant stopVideoCapturer];
        [_participants removeObject:_localParticipant];
        [_participantsCollectionView reloadData];
        _cameraStarted = NO;
    } else {
        [self startCameraPreview];
    }
}

- (IBAction)connectAction:(UIButton *)sender {
    NSError *error;
    [_session connectWith:_token error:&error];
    [sender setHidden: YES];
    [_leaveButton setHidden: NO];
}

- (IBAction)shareButtonTapped:(UIButton *)sender {
    NSLog(@"%@", self.token);
    NSString* shareUrl = [NSString stringWithFormat:@"https://sceenic.watchtogether.test.app/%@", self.token];
    UIPasteboard *board = [UIPasteboard generalPasteboard];
    [board setString:shareUrl];
    UIActivityViewController *activity = [[UIActivityViewController alloc] initWithActivityItems:@[shareUrl] applicationActivities:nil];
    [self presentViewController:activity animated:YES completion:nil];
}

- (IBAction)leaveAction:(id)sender {
    [_session disconnect];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)forceDisconnect:(NSString *)participantId message:(NSString *)message {
    [_session forceDisconnectWithParticipantId:participantId message:@""];
}
//- (IBAction)sendMessageAction:(UIButton *)sender {
//    [_session sendMessageWithMessage:@"123"];
//}

- (IBAction)onChangeLocalParticipantType:(UISegmentedControl *)sender {
    int index = [sender selectedSegmentIndex];
    [_participants removeObject:_localParticipant];
    SessionStateAdapter sessionState = [_session sessionState];
    if (index == 0){
        if (sessionState == SessionStateAdapterConnected){
            [_participants insertObject:_localParticipant atIndex:0];
        }
        _localParticipant.participantType = ParticipantTypeAdapterFullParticipant;
    } else if (index == 1){
        _localParticipant.participantType = ParticipantTypeAdapterViewer;
    } else if (index == 2){
        if (sessionState == SessionStateAdapterConnected){
            [_participants insertObject:_localParticipant atIndex:0];
        }
        _localParticipant.participantType = ParticipantTypeAdapterBroadcaster;
    }
    
    if (sessionState == SessionStateAdapterConnected){
        [_participantsCollectionView reloadData];
    }
}

- (void)setupSession {
    _session = [[SessionAdapter alloc] initWith:_username];
    [_session build];
    _localParticipant = _session.localParticipant;
//    [_localParticipant setWithOrientation: AVCaptureVideoOrientationLandscapeLeft];
// MARK: optional setting -> set to 0 or nil to cancel specific orientation.
//    [_localParticipant setWithOrientation: 0];
    _session.delegate = self;
    [SessionAdapter setMinLogLevel: WTALogLevelDebug];
   
    if (_localParticipantSnapshot != nil) {
        [_localParticipant setWithSnapshot:_localParticipantSnapshot];
        _localParticipantSnapshot = nil;
        if (_localParticipant.participantType != ParticipantTypeAdapterViewer) {
            [self startCameraPreview];
        }
    } else {
        _localParticipant.videoQuality = VideoQualityAdapterDefault;
    }
    _logsVC = [_session createLogsViewController];
}

- (void)startCameraPreview {
    [self checkCameraPermitions];
}

-(void) checkCameraPermitions {
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeVideo completionHandler:^(BOOL granted) {
        }];
        [AVCaptureDevice requestAccessForMediaType:AVMediaTypeAudio completionHandler:^(BOOL granted) {
            [self enableCamera];
        }];
}

-(void) enableCamera {
    dispatch_async(dispatch_get_main_queue(), ^(void){
        if (self->_localParticipant != nil && self->_cameraStarted == NO) {
            NSError* err = nil;
            [self->_localParticipant startCameraPreviewAndReturnError:&err];
            self->_cameraStarted = YES;
            [self->_participants addObject: self-> _localParticipant];
            [self->_participantsCollectionView reloadData];
        }
    });
}

- (void)onSessionErrorWithError:(NSError *)error {
}

- (void)onSessionConnectedWithSessionId:(NSString *)sessionId participants:(NSArray<ParticipantAdapter *> *)participants {
    
}

- (void)onSessionDisconnect {
    _cameraStarted = NO;
    SessionStateAdapter sessionState = [_session sessionState];
    if (sessionState != SessionStateAdapterCreated) {
        _localParticipantSnapshot = [_session.localParticipant takeSnapshot];
    }
    [_participants removeAllObjects];
    _session = nil;
    _localParticipant = nil;
    dispatch_async(dispatch_get_main_queue(), ^(void){
        [_participantsCollectionView reloadData];
     });
    if (_isRegularDisconnect) {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)onDisconnectedWithReason:(NSString *)reason initiator:(NSString *)initiator {
    NSLog(@"message = %@, id = %@", reason, initiator);
    dispatch_async(dispatch_get_main_queue(), ^(void){
        [self.navigationController popViewControllerAnimated:YES];
     });
    
}

- (void)onRemoteParticipantJoinedWithParticipant:(ParticipantAdapter *)participant{
    
}

- (void)onRemoteParticipantStartMediaWithParticipant:(ParticipantAdapter *)participant{
    [_participants addObject:participant];
    [participant setVolume:4.f];
    [_participantsCollectionView reloadData];
}

- (void)onRemoteParticipantStopMediaWithParticipant:(ParticipantAdapter *)participant{
    NSArray* participants = [_participants copy];
    for (ParticipantAdapter* oldParticipant in participants){
        if ([oldParticipant getId] == [participant getId]){
            [_participants removeObject: oldParticipant];
        }
    }
    [_participantsCollectionView reloadData];
}

- (void)onRemoteParticipantLeftWithParticipant:(ParticipantAdapter *)participant{
    NSArray* participants = [_participants copy];
    for (ParticipantAdapter* oldParticipant in participants){
        if ([oldParticipant getId] == [participant getId]){
            [_participants removeObject: oldParticipant];
        }
    }
    [_participantsCollectionView reloadData];
}

-(void)onRemoteParticipantNotificationWithMessage:(NSString *)message participantId:(NSString *)participantId { 
    dispatch_async(dispatch_get_main_queue(), ^(void){
         //Run UI Updates
//        _receiveMessageLabel.text = (@"%@: %@", message, participantId);
     });
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    ParticipantCell* cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ParticipantCell" forIndexPath:indexPath];
    ParticipantAdapter* participant = [_participants objectAtIndex: indexPath.row];
    [cell setParticipant:participant];
    cell.completion = ^(NSString *participantId, NSString *message) {
        [self forceDisconnect:participantId message:message];
    };
    return  cell;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
    return [_participants count];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath{
    float width =  ([[UIScreen mainScreen] bounds].size.width / 2) - 2.0;
    return  CGSizeMake(width, width);
}

// MARK: You can use callObserver to handle incoming call during session
- (void)callObserver:(CXCallObserver *)callObserver callChanged:(CXCall *)call {
//    if (call.hasEnded) {
//        [self callEnded];
//    } else {
//        [self callStarted];
//    }
}

- (void)callEnded {
    [self reconnectFlow];
}

- (void)callStarted {
    [self disconnectFlow];
}
- (void)appMovedToBackground:(NSNotification *) notification {
    [self disconnectFlow];
}

- (void)appDidBecomeActive:(NSNotification *) notification {
    [self reconnectFlow];
}

- (void)reconnectFlow {
    if (_session == nil) {
        [self setupSession];
        NSError *error;
        [_session connectWith:_token error:&error];
        [self startCameraPreview];
    }
}

- (void)disconnectFlow {
    _isRegularDisconnect = NO;
    [_session disconnect];
    [_participants removeAllObjects];
    [_participantsCollectionView reloadData];
}

@end
