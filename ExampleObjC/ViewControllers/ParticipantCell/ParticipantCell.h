//
//  ParticipantCell.h
//  ExampleObjC
//
//  Created by George on 01.04.2021.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@class ParticipantAdapter;

@interface ParticipantCell : UICollectionViewCell
-(void) setParticipant: (ParticipantAdapter*) participant;
@property (nonatomic, copy) void (^completion)(NSString *, NSString *);

@end

NS_ASSUME_NONNULL_END
