//
//  ParticipantCell.m
//  ExampleObjC
//
//  Created by George on 01.04.2021.
//

#import "ParticipantCell.h"
@import AVFoundation;
@import WatchTogetherAdapter;

@interface ParticipantCell() <ParticipantAdapterDelegate, ParticipantAdapterActiveSpeakerDelegate>
@property (weak, nonatomic) IBOutlet UIView *content;
@property (weak, nonatomic) IBOutlet UISlider *videoQuality;
@property (weak, nonatomic) IBOutlet UISlider *frameRate;
@property (weak, nonatomic) IBOutlet UIButton *switchCameraBtn;
@property (weak, nonatomic) IBOutlet UIButton *audioBtn;
@property (weak, nonatomic) IBOutlet UIButton *videoBtn;
@property (weak, nonatomic) IBOutlet UIImageView *qualityIndicator;
@property (weak, nonatomic) IBOutlet UILabel *participantNameLabel;
@property (weak, nonatomic) IBOutlet UISlider *volumeSlider;
@property (weak, nonatomic) IBOutlet UIButton *disconnectUserButton;

@property (weak, nonatomic) ParticipantAdapter* participant;

@property (nonatomic) BOOL isFront;
@end

@implementation ParticipantCell

-(void) setParticipant: (ParticipantAdapter*) participant {
    if (participant){
        _participant = participant;
        _participant.delegate = self;
        _participant.speakerDelegate = self;
        _participantNameLabel.text = [_participant getDisplayName];
        _participantNameLabel.textColor = [UIColor greenColor];
        _isFront = YES;
        [self prepareUI];
        UIView* participantVideo = [_participant getVideo];
        participantVideo.frame = _content.frame;
        [_content addSubview:participantVideo];
        _volumeSlider.value = participant.volume;
        [self updateMedia];
    }
}

-(void) prepareUI {
    [_switchCameraBtn setHidden: ![self isLocal]];
    [_frameRate setHidden: ![self isLocal]];
    [_videoQuality setHidden: ![self isLocal]];
}

-(void) updateMedia {
    NSString* audioImgName = _participant.isAudioEnabled ? @"mic" : @"mic.slash";
    if (@available(iOS 13, *)) {
        UIImage* img = [UIImage systemImageNamed:audioImgName];
        [_audioBtn setImage:img forState:UIControlStateNormal];
    } else {
        // Alternative code for earlier versions of iOS.
    }
    NSString* videoImgName = _participant.isVideoEnabled ? @"video" : @"video.slash";
    if (@available(iOS 13, *)) {
        UIImage* img = [UIImage systemImageNamed:videoImgName];
        [_videoBtn setImage:img forState:UIControlStateNormal];
    } else {
        // Alternative code for earlier versions of iOS.
    }
}

- (IBAction)enableAudio:(UIButton *)sender {
    _participant.isAudioEnabled = !_participant.isAudioEnabled;
    NSString* imgName = _participant.isAudioEnabled ? @"mic" : @"mic.slash";
    if (@available(iOS 13, *)) {
        UIImage* img = [UIImage systemImageNamed:imgName];
        [sender setImage:img forState:UIControlStateNormal];
    } else {
        // Alternative code for earlier versions of iOS.
    }
}
- (IBAction)enableVideo:(UIButton *)sender {
    _participant.isVideoEnabled = !_participant.isVideoEnabled;
    NSString* imgName = _participant.isVideoEnabled ? @"video" : @"video.slash";
    if (@available(iOS 13, *)) {
        UIImage* img = [UIImage systemImageNamed:imgName];
        [sender setImage:img forState:UIControlStateNormal];
    } else {
        // Alternative code for earlier versions of iOS.
    }

}

-(IBAction)disconnnect:(UIButton *)sender {
    NSString *participantId = [_participant getId];
    _completion(participantId, @"");
}

- (IBAction)switchCamera:(id)sender {
    if ([self isLocal]){
        LocalParticipantAdapter* local = _participant;
        _isFront = !_isFront;
        if (_isFront){
        [local setCameraPosition: AVCaptureDevicePositionFront];
        } else {
            [local setCameraPosition: AVCaptureDevicePositionBack];
        }
    }
}
- (IBAction)changeVideoQuality:(UISlider *)sender {
    if ([self isLocal]){
        LocalParticipantAdapter *local = _participant;
        NSInteger videoQuality = (NSInteger) sender.value;
        VideoQualityAdapter *adapter = (VideoQualityAdapter) videoQuality;
        local.videoQuality = adapter;
    }
}

- (IBAction)changeFrameRate:(UISlider *)sender {
    if ([self isLocal]){
        LocalParticipantAdapter *local = _participant;
        local.frameRate = sender.value;
    }
}
- (IBAction)changeVolume:(UISlider *)sender {
    _participant.volume = sender.value;
}

- (BOOL) isLocal{
    return [_participant isKindOfClass:[LocalParticipantAdapter class]];
}

- (void)onReconnecting{
   
}

- (void)onReconnected{
    
}

- (void)onChangeMediaSettings{
    [self updateMedia];
}

- (void)onStreamUpdateWithQualityInfo:(StreamQualityInfoAdapter *)qualityInfo {
    switch(qualityInfo.quality) {
    case StreamQualityAdapterBad:
            _qualityIndicator.backgroundColor = [UIColor redColor];
            _participantNameLabel.textColor = [UIColor redColor];
        break;
    case StreamQualityAdapterGood:
            _qualityIndicator.backgroundColor = [UIColor yellowColor];
            _participantNameLabel.textColor = [UIColor yellowColor];
        break;
    case StreamQualityAdapterExcellent:
            _qualityIndicator.backgroundColor = [UIColor greenColor];
            _participantNameLabel.textColor = [UIColor greenColor];
        break;
    
    
    }
}

- (void)onRemoteParticipantStartSpeaking {
    dispatch_async(dispatch_get_main_queue(), ^(void){
        self.layer.borderWidth = 5;
        self.layer.borderColor = [UIColor blueColor].CGColor;
    });
}

- (void)onRemoteParticipantStopSpeaking {
    dispatch_async(dispatch_get_main_queue(), ^(void){
        self.layer.borderColor = [UIColor clearColor].CGColor;
        self.layer.borderWidth = 0;
    });
}

@end
