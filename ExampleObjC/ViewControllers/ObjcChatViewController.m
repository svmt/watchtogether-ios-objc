//
//  ObjcChatViewController.m
//  ExampleObjC
//
//  Created by Anton Umnitsyn on 02.08.2021.
//

#import "ObjcChatViewController.h"

@interface ObjcChatViewController ()<UITableViewDelegate, UITableViewDataSource> {
    NSArray *messages;
}

@end

@implementation ObjcChatViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    UITableViewCell *cell =  [tableView dequeueReusableCellWithIdentifier:@"ChatCell"];
    return cell;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  messages.count;
}


@end
