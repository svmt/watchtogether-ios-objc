//
//  ObjcLoginViewController.m
//  ExampleObjC
//
//  Created by George on 04.03.2021.
//

#import "ObjcLoginViewController.h"
#import "ObjcSessionViewController.h"

@interface ObjcLoginViewController () {
    NSString *keyUserName;
}

@property (weak, nonatomic) IBOutlet UITextField *userTextField;

@end


@implementation ObjcLoginViewController

NSString* const streamingToken = @"";

- (void)viewDidLoad {
    [super viewDidLoad];
    keyUserName = @"userName";
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController.navigationBar setHidden:YES];
}

- (IBAction)loginAction:(id)sender {
    NSLog(@"Enter to method");
    if  (self.userTextField.text.length == 0) {
        UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Mandatory fields" message:@"Fields can't be empty" preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *action = [UIAlertAction actionWithTitle:@"Ok" style:UIAlertActionStyleCancel handler:nil];
        [self storeCredentials];
        [alert addAction:action];
        [self presentViewController:alert animated:YES completion:nil];
        return;
    }
    
    //use your token
    NSAssert(streamingToken.length > 0, @"Streaming token is empty");
    dispatch_async(dispatch_get_main_queue(), ^{
        ObjcSessionViewController* vc = [self.storyboard instantiateViewControllerWithIdentifier:@"ObjcSessionViewController"];
        [vc setToken:streamingToken];
        [vc setUsername:self.userTextField.text];
        [self.navigationController pushViewController:vc animated:YES];
    });

}

/// Set textfields text to stored data form UserDefaults
- (void)setTextFields {
    NSString *userName = [[NSUserDefaults standardUserDefaults] stringForKey: keyUserName];
    if (userName.length > 0) {
        self.userTextField.text = userName;
    }
}


/// Store text fields data in UserDefaults
- (void)storeCredentials {
    [[NSUserDefaults standardUserDefaults] setValue:self.userTextField.text forKey: keyUserName];
}

- (IBAction)tapAction:(id)sender {
    [self.view endEditing:YES];
}

@end
