//
//  ObjcSessionViewController.h
//  Example
//
//  Created by George on 07.12.2020.
//

#import <UIKit/UIKit.h>
#import <CallKit/CallKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ObjcSessionViewController : UIViewController <CXCallObserverDelegate>
@property (weak, nonatomic) IBOutlet UICollectionView *participantsCollectionView;
@property (nonatomic) NSString* token;
@property (nonatomic) NSString* username;
@end

NS_ASSUME_NONNULL_END
